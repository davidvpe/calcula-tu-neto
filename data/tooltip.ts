type RowTypeBase =
  | 'SUELDO_BRUTO_MENSUAL'
  | 'OTROS_INGRESOS_MENSUAL'
  | 'SUELDO_BRUTO_ANUAL'
  | 'GRATIFICACIONES'
  | 'BENEFICIO_7_UITS'
  | 'REMUNERACION_FISCAL_ANUAL'
  | 'IR_TOTAL_ANUAL'
  | 'IR_MENSUAL'
  | 'AFP_BASE'
  | 'AFP_COMISION'
  | 'AFP_PRIMA'
  | 'AFP_TOTAL'
  | 'TOTAL_DESCUENTOS'
  | 'SUELD_NETO_MENSUAL'
  | 'SUELDO_NETO_ANUAL'
  | 'PORCENTAJE_NETO'

type RowWithDynamicIR = RowTypeBase | `IR_SEG_${number}`
type RowWithFixedIR = RowTypeBase | 'IR_SEG_1' | 'IR_SEG_2' | 'IR_SEG_3' | 'IR_SEG_4' | 'IR_SEG_5'
export type RowType = RowWithDynamicIR | RowWithFixedIR

export const getTitle = (rowType: RowType | null) => {
  switch (rowType) {
    case 'SUELDO_BRUTO_MENSUAL':
      return 'Sueldo bruto mensual'
    case 'OTROS_INGRESOS_MENSUAL':
      return 'Otros ingresos mensuales'
    case 'SUELDO_BRUTO_ANUAL':
      return 'Sueldo bruto anual'
    case 'GRATIFICACIONES':
      return 'Gratificaciones'
    case 'BENEFICIO_7_UITS':
      return 'Beneficio 7 UITS'
    case 'REMUNERACION_FISCAL_ANUAL':
      return 'Remuneración fiscal anual'
    case 'REMUNERACION_NETA_ANUAL':
      return 'Remuneración neta anual'
    case 'IR_SEG_1':
      return 'IR Segmento 1 (0 UIT - 5 UIT) - 8%'
    case 'IR_SEG_2':
      return 'IR Segmento 2 (5 UIT - 20 UIT) - 14%'
    case 'IR_SEG_3':
      return 'IR Segmento 3 (20 UIT - 35 UIT) - 17%'
    case 'IR_SEG_4':
      return 'IR Segmento 4 (35 UIT - 45 UIT) - 20%'
    case 'IR_SEG_5':
      return 'IR Segmento 5 (45 UIT o más) - 30%'
    case 'IR_TOTAL_ANUAL':
      return 'Impuesto a la renta anual'
    case 'IR_MENSUAL':
      return 'Impuesto a la renta mensual'
    case 'AFP_BASE':
      return 'Aporte a la AFP'
    case 'AFP_COMISION':
      return 'Comisión de la AFP'
    case 'AFP_PRIMA':
      return 'Prima de la AFP'
    case 'AFP_TOTAL':
      return 'Total descuento AFP'
    case 'SUELD_NETO_MENSUAL':
      return 'Sueldo neto mensual'
    case 'PORCENTAJE_NETO':
      return 'Porcentaje del sueldo'
    case 'SUELDO_NETO_ANUAL':
      return 'Sueldo neto anual'
    case 'TOTAL_DESCUENTOS':
      return 'Total descuentos'
    default:
      return ''
  }
}

export const getDescription = (rowType: RowType | null) => {
  switch (rowType) {
    case 'SUELDO_BRUTO_MENSUAL':
      return 'Este es tu sueldo bruto mensual'
    case 'OTROS_INGRESOS_MENSUAL':
      return 'Estos son otros ingresos que otorga la empresa como tarjetas de alimentacion, movilidad, etc. Este monto es mensual, por lo que si no es mensual, debes calcular el total que recibes de este benficio al año y luego dividirlo por 12.'
    case 'SUELDO_BRUTO_ANUAL':
      return 'Este es tu sueldo multiplicado por los 12 meses que esperas trabajar en el año junto a las gratificaciones y otros ingresos que la empresa pueda dar que no esten dentro de tu sueldo base'
    case 'GRATIFICACIONES':
      return 'Este es el total de gratificaciones que vas a percibir incluyendo el 8% de ESSALUD'
    case 'BENEFICIO_7_UITS':
      return 'Este es un beneficio que otorga la SUNAT en el calculo del impuesto a la renta, te descuentan hasta 7 UITs'
    case 'REMUNERACION_FISCAL_ANUAL':
      return 'Este es el total de tu sueldo bruto anual más tus gratificaciones menos el beneficio de 7 UITs'
    case 'IR_SEG_1':
      return 'Este es el impuesto a la renta que pagas en el primer segmento (0 UIT - 5 UIT) y es del 8%'
    case 'IR_SEG_2':
      return 'Este es el impuesto a la renta que pagas en el segundo segmento (5 UIT - 20 UIT) y es del 14%'
    case 'IR_SEG_3':
      return 'Este es el impuesto a la renta que pagas en el tercer segmento (20 UIT - 35 UIT) y es del 17%'
    case 'IR_SEG_4':
      return 'Este es el impuesto a la renta que pagas en el cuarto segmento (35 UIT - 45 UIT) y es del 20%'
    case 'IR_SEG_5':
      return 'Este es el impuesto a la renta que pagas en el quinto segmento (45 UIT o más) y es del 30%'
    case 'IR_TOTAL_ANUAL':
      return 'Este es el total de impuesto a la renta que pagas en el año'
    case 'IR_MENSUAL':
      return 'Este es el total de impuesto a la renta que pagas en el mes'
    case 'AFP_BASE':
      return 'Este es el aporte que haces a tu AFP'
    case 'AFP_COMISION':
      return 'Este es el porcentaje de comisión que cobra tu AFP'
    case 'AFP_PRIMA':
      return 'Este es el porcentaje de prima que cobra tu AFP'
    case 'AFP_TOTAL':
      return 'Este es el total de descuento que se te hace por tu AFP'
    case 'SUELDO_NETO_ANUAL':
      return 'Este es tu sueldo neto anual'
    case 'TOTAL_DESCUENTOS':
      return 'Este es el total de descuentos que se te hacen mensualmente'
    case 'SUELD_NETO_MENSUAL':
      return 'Este es el total de tu sueldo neto mensual'
    case 'PORCENTAJE_NETO':
      return 'Este es el porcentaje de tu sueldo neto mensual respecto a tu sueldo bruto mensual'
  }
}
