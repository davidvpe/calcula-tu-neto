import { AFPName, getAFP, PORCENTAJE_ESSALUD, getLimitesYPorcentajesDeSegmentos, getUIT } from './values'

export const getSueldoBrutoAnual = (sueldoBrutoMensual: number, otrosIngresos?: number): number => {
  return (
    (sueldoBrutoMensual + (otrosIngresos === undefined ? 0 : otrosIngresos)) * 12 +
    getGratificaciones(sueldoBrutoMensual)
  )
}

export const getGratificaciones = (sueldoBrutoMensual: number): number => {
  return sueldoBrutoMensual * (1 + PORCENTAJE_ESSALUD) * 2
}

export const getBeneficio7Uits = (sueldoBrutoMensual: number, otrosIngresos?: number): number => {
  const currentYear = new Date().getFullYear()
  const totalBeneficio = getUIT(currentYear) * 7
  console.log('totalBeneficio', totalBeneficio)
  const sueldoFiscalAnual = getSueldoBrutoAnual(sueldoBrutoMensual, otrosIngresos)
  console.log('sueldoFiscalAnual', sueldoFiscalAnual)
  return totalBeneficio > sueldoFiscalAnual ? sueldoFiscalAnual : totalBeneficio
}

export const getSueldoFiscalAnual = (sueldoBrutoMensual: number, otrosIngresos?: number): number => {
  const totalFiscal = getSueldoBrutoAnual(sueldoBrutoMensual, otrosIngresos)
  const beneficios = getBeneficio7Uits(sueldoBrutoMensual, otrosIngresos)
  if (totalFiscal - beneficios <= 0) {
    return 0
  } else {
    return totalFiscal - beneficios
  }
}

export const getSegmentos = (sueldoBrutoMensual: number, otrosIngresos?: number): number[] => {
  const sueldoFiscalAnual = getSueldoFiscalAnual(sueldoBrutoMensual, otrosIngresos)
  return getSegmento(0, sueldoFiscalAnual, [])
}

export const getIRTotal = (sueldoBrutoMensual: number, otrosIngresos?: number) => {
  const segmentos = getSegmentos(sueldoBrutoMensual, otrosIngresos)
  return segmentos.reduce((total, segmento) => total + segmento, 0)
}

export const getIRMensual = (sueldoBrutoMensual: number, otrosIngresos?: number) => {
  return getIRTotal(sueldoBrutoMensual, otrosIngresos) / 12
}

export const getAFPBase = (sueldoBrutoMensual: number, AFP: AFPName): number => {
  return (getAFP(AFP).base / 100) * sueldoBrutoMensual
}

export const getAFPComision = (sueldoBrutoMensual: number, AFP: AFPName): number => {
  return (getAFP(AFP).comision / 100) * sueldoBrutoMensual
}

export const getAFPPrima = (sueldoBrutoMensual: number, AFP: AFPName): number => {
  return (getAFP(AFP).prima / 100) * sueldoBrutoMensual
}

export const getAFPTotal = (sueldoBrutoMensual: number, AFP: AFPName): number => {
  return (
    getAFPBase(sueldoBrutoMensual, AFP) + getAFPComision(sueldoBrutoMensual, AFP) + getAFPPrima(sueldoBrutoMensual, AFP)
  )
}

export const getTotalDescuentosMensual = (sueldoBrutoMensual: number, AFP: AFPName, otrosIngresos?: number): number => {
  return getAFPTotal(sueldoBrutoMensual, AFP) + getIRMensual(sueldoBrutoMensual, otrosIngresos)
}

export const getSueldoNetoMensual = (sueldoBrutoMensual: number, AFP: AFPName, otrosIngresos?: number): number => {
  return (
    sueldoBrutoMensual +
    (otrosIngresos != undefined ? otrosIngresos : 0) -
    getAFPTotal(sueldoBrutoMensual, AFP) -
    getIRMensual(sueldoBrutoMensual, otrosIngresos)
  )
}

export const getSueldoNetoAnual = (sueldoBrutoMensual: number, AFP: AFPName, otrosIngresos?: number): number => {
  return getSueldoNetoMensual(sueldoBrutoMensual, AFP, otrosIngresos) * 12
}

export const getPorcentajeSueldoNeto = (sueldoBrutoMensual: number, AFP: AFPName, otrosIngresos?: number): number => {
  return (
    getSueldoNetoMensual(sueldoBrutoMensual, AFP, otrosIngresos) /
    (sueldoBrutoMensual + (otrosIngresos === undefined ? 0 : otrosIngresos))
  )
}

const getSegmento = (montoYaCalculado: number, montoRestante: number, segmentos: number[]): number[] => {
  if (montoRestante <= 0) {
    return segmentos
  }

  const currentYear = new Date().getFullYear()
  const UIT = getUIT(currentYear)

  const segmentoActual = getLimitesYPorcentajesDeSegmentos(UIT)[segmentos.length]

  if (montoYaCalculado + montoRestante <= segmentoActual.maxOrEqual) {
    return [...segmentos, (montoRestante * segmentoActual.porcentaje) / 100]
  } else {
    if (segmentoActual.maxOrEqual === -1) {
      return [...segmentos, (montoRestante * segmentoActual.porcentaje) / 100]
    } else {
      const montoParaCalculo = segmentoActual.maxOrEqual - montoYaCalculado

      const rentaCalculada = (montoParaCalculo * segmentoActual.porcentaje) / 100

      console.log('======')
      console.log('montoYaCalculado', montoYaCalculado)
      console.log('montoRestante', montoRestante)
      console.log('segmentoActual.maxOrEqual', segmentoActual.maxOrEqual)
      console.log('montoParaCalculo', montoParaCalculo)
      console.log('rentaCalculada', rentaCalculada)

      return getSegmento(montoYaCalculado + montoParaCalculo, montoRestante - montoParaCalculo, [
        ...segmentos,
        rentaCalculada
      ])
    }
  }
}
