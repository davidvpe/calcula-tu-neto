export type AFPName =
  | "AFP Habitat"
  | "AFP Profuturo"
  | "AFP Prima"
  | "AFP Integra"
  | "ONP";

type AFP = {
  nombre: AFPName;
  base: number;
  comision: number;
  prima: number;
};

export const AFPs: AFP[] = [
  {
    nombre: "AFP Habitat",
    base: 10,
    comision: 1.47,
    prima: 1.74,
  },
  {
    nombre: "AFP Integra",
    base: 10,
    comision: 1.55,
    prima: 1.74,
  },
  {
    nombre: "AFP Prima",
    base: 10,
    comision: 1.6,
    prima: 1.74,
  },
  {
    nombre: "AFP Profuturo",
    base: 10,
    comision: 1.6,
    prima: 1.74,
  },
  {
    nombre: "ONP",
    base: 13,
    comision: 0,
    prima: 0,
  },
];

export const NombresAFP = AFPs.map((afp) => afp.nombre);

export const getAFP = (nombre: AFPName): AFP => {
  return AFPs.find((afp) => afp.nombre === nombre) as AFP;
};

// export const UIT = 4600

export const getUIT = (year: number): number => {
  switch (year) {
    case 2025:
      return 5350;
    case 2024:
      return 5150;
    case 2023:
      return 4950;
    case 2022:
      return 4600;
    case 2021:
      return 4400;
    case 2020:
      return 4300;
    case 2019:
      return 4200;
    default:
      return getUIT(year - 1);
  }
};

export const PORCENTAJE_ESSALUD = 8 / 100;
export const getLimitesYPorcentajesDeSegmentos = (UIT: number) => [
  {
    maxOrEqual: 5 * UIT,
    porcentaje: 8,
  },
  {
    maxOrEqual: 20 * UIT,
    porcentaje: 14,
  },
  {
    maxOrEqual: 35 * UIT,
    porcentaje: 17,
  },
  {
    maxOrEqual: 45 * UIT,
    porcentaje: 20,
  },
  {
    maxOrEqual: -1,
    porcentaje: 30,
  },
];
