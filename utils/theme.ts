import { extendTheme } from '@chakra-ui/react'

export const theme = extendTheme({
  breakpoints: {
    sm: '320px',
    md: '768px',
    lg: '960px',
    xl: '1200px'
  },
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false
  }
})
