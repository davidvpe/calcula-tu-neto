export const parseMoney = (number: string | number) => {
  return new Intl.NumberFormat('es-PE', {
    style: 'currency',
    currency: 'PEN'
  }).format(Number(number))
}

export const parsePercentage = (number: string | number) => {
  return new Intl.NumberFormat('es-PE', {
    style: 'percent',
    minimumFractionDigits: 2
  }).format(Number(number))
}
