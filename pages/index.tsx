import type { NextPage } from 'next'
import { Flex, HStack, InputGroup, InputLeftAddon, Link, Switch, Text, useColorMode } from '@chakra-ui/react'
import { Heading } from '@chakra-ui/react'
import { Select } from '@chakra-ui/react'
import { Input } from '@chakra-ui/react'
import { AFPName, getUIT, NombresAFP } from '../data/values'
import { useState } from 'react'
import ResultsTable from '../components/ResultsTable'
import { parseMoney } from '../utils/formatter'

import { AiFillGitlab, AiFillLinkedin, AiOutlineGithub, AiOutlineMail, AiOutlineTwitter } from 'react-icons/ai'

import { BsFillMoonFill, BsFillSunFill } from 'react-icons/bs'

const Home: NextPage = () => {
  const [afp, setAfp] = useState<AFPName>()
  const [sueldo, setSueldo] = useState<string>()
  const [otrosIngresos, setOtrosIngresos] = useState<string>()
  const currentYear = new Date().getFullYear()
  const UITActual = getUIT(currentYear)

  const { colorMode, toggleColorMode } = useColorMode()

  const otrosIngresosValidado = isNaN(Number(otrosIngresos)) ? 0 : Number(otrosIngresos)
  const sueldoValidado = isNaN(Number(sueldo)) ? 0 : Number(sueldo)

  return (
    <Flex
      gap={'2em'}
      direction="column"
      marginX={{
        sm: '0.5em',
        md: '2em',
        lg: '3em',
        xl: '5em'
      }}
      marginY={{
        sm: '1em',
        md: '2em',
        lg: '3em',
        xl: '5em'
      }}
    >
      <HStack>
        <Heading flex="1" size={'2xl'}>
          Calcula tu neto!
        </Heading>
        <HStack direction="row" fontSize={'1.2em'} gap="0.5em">
          <BsFillSunFill />
          <Switch
            colorScheme="teal"
            size="lg"
            isChecked={colorMode === 'dark'}
            onChange={() => {
              toggleColorMode()
            }}
          />
          <BsFillMoonFill />
        </HStack>
      </HStack>

      <Flex
        gap={'1em'}
        direction={{
          sm: 'column',
          md: 'row'
        }}
      >
        <Select
          placeholder="Que AFP tienes?"
          maxWidth={'xs'}
          onChange={(e) => setAfp(e.target.value as AFPName)}
          defaultValue={afp}
        >
          {NombresAFP.map((n, i) => (
            <option key={`afp-${i}`} value={n}>
              {n}
            </option>
          ))}
        </Select>
        <InputGroup maxWidth={'xs'}>
          <InputLeftAddon>S/.</InputLeftAddon>
          <Input
            value={sueldo || ''}
            type="tel"
            placeholder="Sueldo mensual"
            onChange={(e) => setSueldo(e.target.value)}
          />
        </InputGroup>
        <InputGroup maxWidth={'xs'}>
          <InputLeftAddon>S/.</InputLeftAddon>
          <Input
            value={otrosIngresos || ''}
            type="tel"
            placeholder="Otros ingresos mensuales"
            onChange={(e) => setOtrosIngresos(e.target.value)}
          />
        </InputGroup>
      </Flex>
      <ResultsTable afp={afp} sueldo={sueldoValidado} otrosIngresos={otrosIngresosValidado} />
      <Text fontStyle={'italic'} textColor={'gray.500'}>{`Valor de la UIT para el año ${currentYear} : ${parseMoney(
        UITActual
      )}`}</Text>

      <Text fontStyle={'italic'} textColor={'gray.500'} marginTop={'-1em'}>
        {`Elaborado por David Velarde`}
      </Text>
      <HStack fontSize={'1.5em'} marginTop={'-0.5em'}>
        <Link href="https://gitlab.com/davidvpe" target="_blank" referrerPolicy="no-referrer">
          <AiFillGitlab />
        </Link>
        <Link href="https://github.com/davidvpe" target="_blank" referrerPolicy="no-referrer">
          <AiOutlineGithub />
        </Link>
        <Link href="https://twitter.com/davidvpe" target="_blank" referrerPolicy="no-referrer">
          <AiOutlineTwitter />
        </Link>
        <Link href="https://linkedin.com/in/davidmvr" target="_blank" referrerPolicy="no-referrer">
          <AiFillLinkedin />
        </Link>
        <Link href="mailto:david.velarde@copstone.net" target="_blank" referrerPolicy="no-referrer">
          <AiOutlineMail />
        </Link>
      </HStack>
    </Flex>
  )
}

export default Home
