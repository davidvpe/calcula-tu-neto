import { HStack, Stack, Text } from '@chakra-ui/react'
import React from 'react'
import { AiOutlineInfoCircle } from 'react-icons/ai'

type Props = {
  title: string
  description: string
  onTooltipClick?: () => void
}

const ResultRow = ({ title, description, onTooltipClick }: Props) => {
  return (
    <Stack direction="row">
      <HStack flex="1" maxW="65%">
        {onTooltipClick && <AiOutlineInfoCircle onClick={onTooltipClick} fontSize="1.2em" cursor="pointer" />}
        <Text>{title}</Text>
      </HStack>

      <Text maxW="35%">{description}</Text>
    </Stack>
  )
}

export default ResultRow
