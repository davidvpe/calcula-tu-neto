import { StackDivider, VStack } from '@chakra-ui/react'
import React from 'react'
import {
  getAFPBase,
  getAFPComision,
  getAFPPrima,
  getAFPTotal,
  getBeneficio7Uits,
  getGratificaciones,
  getIRMensual,
  getIRTotal,
  getPorcentajeSueldoNeto,
  getSegmentos,
  getSueldoBrutoAnual,
  getSueldoFiscalAnual,
  getSueldoNetoAnual,
  getSueldoNetoMensual,
  getTotalDescuentosMensual
} from '../data/calculos'
import { getTitle, RowType } from '../data/tooltip'
import { AFPName } from '../data/values'
import { parseMoney, parsePercentage } from '../utils/formatter'
import ResultRow from './ResultRow'

type Props = {
  sueldo: number
  otrosIngresos?: number
  afp: AFPName
  setTooltipRow: (row: RowType | null) => void
}

const CalculoBase = (props: Props) => {
  const sueldoBrutoMensual = props.sueldo

  const otrosIngresosMensuales = props.otrosIngresos

  console.log('otrosingresos', otrosIngresosMensuales)

  const gratificaciones = getGratificaciones(props.sueldo)

  const sueldoBrutoAnual = getSueldoBrutoAnual(props.sueldo, otrosIngresosMensuales)

  const sueldoFiscalAnual = getSueldoFiscalAnual(props.sueldo, otrosIngresosMensuales)

  const irMensual = getIRMensual(props.sueldo, otrosIngresosMensuales)
  const irTotal = getIRTotal(props.sueldo, otrosIngresosMensuales)

  const beneficio7Uits = getBeneficio7Uits(props.sueldo, otrosIngresosMensuales)
  const segmentos = getSegmentos(props.sueldo, otrosIngresosMensuales)

  const afpBase = getAFPBase(props.sueldo, props.afp)
  const afpComision = getAFPComision(props.sueldo, props.afp)
  const afpPrima = getAFPPrima(props.sueldo, props.afp)
  const afpTotal = getAFPTotal(props.sueldo, props.afp)

  const sueldoNetoMensual = getSueldoNetoMensual(props.sueldo, props.afp, otrosIngresosMensuales)
  const sueldoNetoAnual = getSueldoNetoAnual(props.sueldo, props.afp, otrosIngresosMensuales)
  const porcentajeSueldoNeto = getPorcentajeSueldoNeto(props.sueldo, props.afp, otrosIngresosMensuales)

  const totalDescuentos = getTotalDescuentosMensual(props.sueldo, props.afp, otrosIngresosMensuales)

  return (
    <VStack spacing={4} align="stretch">
      {sueldoBrutoMensual > 0 && (
        <ResultRow
          title="Sueldo bruto mensual"
          description={parseMoney(props.sueldo)}
          onTooltipClick={() => props.setTooltipRow('SUELDO_BRUTO_MENSUAL')}
        ></ResultRow>
      )}

      {gratificaciones > 0 && (
        <ResultRow
          title="Gratificaciones anuales"
          description={parseMoney(gratificaciones)}
          onTooltipClick={() => props.setTooltipRow('GRATIFICACIONES')}
        ></ResultRow>
      )}

      {otrosIngresosMensuales && otrosIngresosMensuales > 0 && (
        <ResultRow
          title="Otros ingresos mensuales"
          description={parseMoney(otrosIngresosMensuales)}
          onTooltipClick={() => props.setTooltipRow('OTROS_INGRESOS_MENSUAL')}
        ></ResultRow>
      )}

      {sueldoBrutoAnual > 0 && (
        <ResultRow
          title="Sueldo bruto anual"
          description={parseMoney(sueldoBrutoAnual)}
          onTooltipClick={() => props.setTooltipRow('SUELDO_BRUTO_ANUAL')}
        ></ResultRow>
      )}

      <StackDivider borderColor="gray.400" borderWidth={'0.01em'} />

      {irTotal > 0 && sueldoBrutoMensual > 0 && (
        <ResultRow
          title="Descuento de hasta 7 UITs"
          description={parseMoney(beneficio7Uits)}
          onTooltipClick={() => props.setTooltipRow('BENEFICIO_7_UITS')}
        ></ResultRow>
      )}

      {sueldoFiscalAnual > 0 && (
        <ResultRow
          title="Sueldo fiscal anual"
          description={parseMoney(sueldoFiscalAnual)}
          onTooltipClick={() => props.setTooltipRow('REMUNERACION_FISCAL_ANUAL')}
        ></ResultRow>
      )}

      {sueldoFiscalAnual > 0 &&
        segmentos.map((segmento, i) => (
          <ResultRow
            key={`segmento-${i}`}
            title={getTitle(`IR_SEG_${i + 1}`)}
            description={parseMoney(segmento)}
            onTooltipClick={() => props.setTooltipRow(`IR_SEG_${i + 1}`)}
          ></ResultRow>
        ))}

      {irTotal > 0 && (
        <ResultRow
          title="Impuesto a la renta anual"
          description={parseMoney(irTotal)}
          onTooltipClick={() => props.setTooltipRow('IR_TOTAL_ANUAL')}
        ></ResultRow>
      )}

      {irMensual && (
        <>
          <ResultRow
            title="Impuesto a la renta mensual"
            description={parseMoney(irMensual)}
            onTooltipClick={() => props.setTooltipRow('IR_MENSUAL')}
          ></ResultRow>
          <StackDivider borderColor="gray.400" borderWidth={'0.01em'} />
        </>
      )}

      {afpBase > 0 && (
        <ResultRow
          title="Aporte a la AFP"
          description={parseMoney(getAFPBase(props.sueldo, props.afp))}
          onTooltipClick={() => props.setTooltipRow('AFP_BASE')}
        ></ResultRow>
      )}

      {afpComision > 0 && (
        <ResultRow
          title="Comisión de la AFP"
          description={parseMoney(afpComision)}
          onTooltipClick={() => props.setTooltipRow('AFP_COMISION')}
        ></ResultRow>
      )}

      {afpPrima > 0 && (
        <ResultRow
          title="Prima de la AFP"
          description={parseMoney(afpPrima)}
          onTooltipClick={() => props.setTooltipRow('AFP_PRIMA')}
        ></ResultRow>
      )}

      {afpTotal > 0 && (
        <ResultRow
          title="Total aportes a la AFP"
          description={parseMoney(afpTotal)}
          onTooltipClick={() => props.setTooltipRow('AFP_TOTAL')}
        ></ResultRow>
      )}
      <StackDivider borderColor="gray.400" borderWidth={'0.01em'} />

      {totalDescuentos > 0 && (
        <ResultRow
          title="Total descuentos"
          description={parseMoney(totalDescuentos)}
          onTooltipClick={() => props.setTooltipRow('TOTAL_DESCUENTOS')}
        ></ResultRow>
      )}
      <StackDivider borderColor="gray.400" borderWidth={'0.01em'} />

      {sueldoNetoMensual > 0 && (
        <ResultRow
          title="Sueldo neto mensual"
          description={parseMoney(sueldoNetoMensual)}
          onTooltipClick={() => props.setTooltipRow('SUELD_NETO_MENSUAL')}
        ></ResultRow>
      )}

      {sueldoNetoAnual > 0 && (
        <ResultRow
          title="Sueldo neto anual"
          description={parseMoney(sueldoNetoAnual)}
          onTooltipClick={() => props.setTooltipRow('SUELDO_NETO_ANUAL')}
        ></ResultRow>
      )}

      <StackDivider borderColor="gray.400" borderWidth={'0.01em'} />
      {porcentajeSueldoNeto > 0 && (
        <ResultRow
          title="Porcentaje de sueldo neto"
          description={parsePercentage(porcentajeSueldoNeto)}
          onTooltipClick={() => props.setTooltipRow('PORCENTAJE_NETO')}
        ></ResultRow>
      )}
    </VStack>
  )
}

export default CalculoBase
