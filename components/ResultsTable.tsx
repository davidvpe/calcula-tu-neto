import {
  Box,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  StackDivider,
  Text,
  VStack
} from '@chakra-ui/react'
import React from 'react'
import { parseMoney } from '../utils/formatter'
import ResultRow from './ResultRow'
import { getDescription, getTitle, RowType } from '../data/tooltip'
import CalculoBase from './CalculoBase'
import { AFPName } from '../data/values'

type Props = {
  afp?: AFPName
  sueldo?: number
  otrosIngresos?: number
}

const Message = (props: { message: string }) => {
  return (
    <Box width="full">
      <Text color={'gray.400'} fontSize="md">
        {props.message}
      </Text>
    </Box>
  )
}

const ResultsTable = (props: Props) => {
  const { afp, sueldo } = props

  const [tooltipRow, setTooltipRow] = React.useState<RowType | null>(null)

  if (!afp) {
    return <Message message="Por favor selecciona tu AFP" />
  }

  if (!afp || !sueldo) {
    return <Message message="Aqui aparecera tu corrida anual de aportes y sueldo neto" />
  }

  return (
    <>
      <VStack
        boxShadow={'xl'}
        border={'1px'}
        borderColor={'gray.200'}
        paddingX={{
          sm: '1em',
          base: '3em'
        }}
        paddingY={'1em'}
        rounded={'md'}
        spacing={4}
        align="stretch"
      >
        <CalculoBase setTooltipRow={setTooltipRow} sueldo={sueldo} afp={afp} otrosIngresos={props.otrosIngresos} />
      </VStack>
      <Modal isOpen={tooltipRow != null} onClose={() => setTooltipRow(null)}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{getTitle(tooltipRow)}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>{getDescription(tooltipRow)}</ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={() => setTooltipRow(null)}>
              Cerrar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default ResultsTable
